from django.http import HttpResponse
from django.shortcuts import render,redirect
from .forms import ContactForm, LoginForm, RegisterForm
from django.contrib.auth import authenticate, login, get_user_model

def home_page(request):
	context = {
		"title":"Legal Gate !",
		"content":"Welcome to home page",
	}
	if request.user.is_authenticated():
		context["premium_content"]="YEAHHHH"
	return render(request, "home_page.html", context)

def about_page(request):
	context = {
		"title":"About Legal Gate!",
	}
	return render(request, "home_page.html", context)

def contact_page(request):
	contact_form = ContactForm(request.POST or None)
	context = {
		"title":"Contact Legal Gate !",
		"form" : contact_form
	}
	if contact_form.is_valid():
		print(contact_form.cleaned_data)
	return render(request, "contact/view.html", context)

def login_page(request):

	form = LoginForm(request.POST or None)
	context = {
		"title":"Login Legal Gate !",
		"form" : form
	}
	print("User loged-in")
	if form.is_valid():
		print(form.cleaned_data)
		username = form.cleaned_data.get("username")
		password = form.cleaned_data.get("password")
		user = authenticate(request, username = username, password = password)
		if user is not None:
			login(request, user)
			return redirect("/")
		else:
			print("Error")
	return render(request, "auth/login.html", context)
User = get_user_model()
def register_page(request):
	form = RegisterForm(request.POST or None)
	context = {
		"title":"Register Legal gate !",
		"form" : form
	}
	if form.is_valid():
		print(form.cleaned_data)
		email = form.cleaned_data.get("email")
		username = form.cleaned_data.get("username")
		password = form.cleaned_data.get("password1")
		new_user = User.Objects.create_user(email, username, password)
		print(new_user)
	return render(request, "auth/register.html", context)


