from django import forms
class ContactForm(forms.Form):
	fullname = forms.CharField(
		widget=forms.TextInput(
			attrs={
			"class":"form-control", 
			"placeholder": "Your full name"
			}
			)
		)
	email   = forms.EmailField(
		widget=forms.EmailInput(
			attrs={
			"class":"form-control", 
			"placeholder": "Your email"
			}
			)
		)

	content = forms.CharField(
		widget=forms.Textarea(
			attrs={
			"class": "form-control",
			"placeholder": "Your message"
			}
			)
		)

	def clean_email(self):
		email = self.cleaned_data.get("email")
		if not "gmail.com" in email:
			raise forms.ValidationError("Email has to be gmail.com")
		return email
class LoginForm(forms.Form):
	username = forms.CharField()
	password = forms.CharField(widget=forms.PasswordInput)

class RegisterForm(forms.Form):
	email   = forms.EmailField()
	username = forms.CharField()
	password1 = forms.CharField(label='Your Password', widget=forms.PasswordInput)
	password2 = forms.CharField(label='Confirm Your Password', widget=forms.PasswordInput)
	def clean_email(self):
		email = self.cleaned_data.get("email")
		if not "gmail.com" in email:
			raise forms.ValidationError("Email has to be gmail.com")
		return email
	def clean_password(self):
		password = self.cleaned_data
		password1 = self.cleaned_data.get('password1')
		password2 = self.cleaned_data.get('password2')
		if password1 != password2:
			raise forms.ValidationError("Passwords don't match")
		return password